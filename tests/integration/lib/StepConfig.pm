#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(
                    $server
                    $resourcePaths
                    getResourcePath
                    $authCredentials
                    $requestData
                    $authInfo
                    $dbh
                    $bannerDbh
              );

our $server = 'http://ws/api';

our $resourcePaths = {
    'authentication' => '/authentication/v1',
};

sub getResourcePath {
    my $pathKey = shift;
    my $values = shift;

    my $path = '';

    if (defined($resourcePaths->{$pathKey})) {
        $path = $resourcePaths->{$pathKey};

        if ($values && ref($values) eq 'HASH') {
            foreach my $key ( keys %{$values}) {
                $path =~ s/\{$key\}/$values->{$key}/g;
            }
        }
    }

    return $path;
}

our $authCredentials = {
    'RESTngTest' => {
                     'username' => 'RESTUSER',
                     'password' => 'bubba',
                 },
    'doej' => {
                     'username' => 'doej',
                     'password' => 'doej',
                 },
    'smithd' => {
                     'username' => 'smithd',
                     'password' => 'smithd',
                 },
};

our $requestData = {};

our $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

our $bannerDbh = DBI->connect("DBI:Oracle:XE","baninst1","Hello123") || die DBI->errstr;

1;