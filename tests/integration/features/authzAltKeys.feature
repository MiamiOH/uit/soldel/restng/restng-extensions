# Test the Extension\MUUser class to validate SELF authentication with alternate keys
Feature: Authorize access as SELF when using alternate keys
  As a developer using RESTng authorization
  I want to authorize access as SELF when using alternate keys
  In order to support consumers using different person id types

  Background:
    Given the test data is ready

  Scenario: Require authentication to access the resource
    Given a REST client
    When I make a GET request for /extension/person/doej
    Then the HTTP status code is 401

  Scenario: Allow access using the default key
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/doej
    Then the HTTP status code is 200

  Scenario: Do not allow access for another user
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/smithd
    Then the HTTP status code is 401

  Scenario: Allow access using an explicit uid key
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/uid=doej
    Then the HTTP status code is 200

  Scenario: Allow access using an explicit uniqueId key
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/uniqueId=doej
    Then the HTTP status code is 200

  Scenario: Allow access using an alternate key
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/pidm=123456
    Then the HTTP status code is 200

  Scenario: Do not allow access for another user
    Given a REST client
    And a token for the smithd user
    When I make a GET request for /extension/person/pidm=123456
    Then the HTTP status code is 401

