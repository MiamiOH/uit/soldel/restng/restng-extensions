#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {

    my $idData = loadDataFromFile('ids');
    foreach my $table (qw(SZBUNIQ)) {
        $bannerDbh->do(qq{
                delete from $table
            });
    }

    foreach my $record (@{$idData}) {
        $bannerDbh->do(q{
            insert into szbuniq (szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id)
                values (?,?,?)
            }, undef, $record->{'pidm'}, $record->{'banner_id'}, uc $record->{'uid'});
    }
};

sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];

    open FILE, "../sql/sampleData/$fileName.txt" or die "Couldn't open ../sql/sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;

    my @fieldNames = split("\t", $names);
    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}