# Test the Extension\MUUser class to validate SELF authentication with alternate keys
Feature: Lookup Banner ID using uniqueid, PIDM or Banner ID
  As a developer using Banner identities
  I want to look up Banner person identifiers in various ways
  In order to provide flexible person oriented resources

  Background:
    Given the test data is ready

  Scenario: Look up a Banner person by default uniqueid
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element uniqueId equal to "doej"
    And the subject has an element pidm equal to "123456"
    And the subject has an element bannerId equal to "+00123456"

  Scenario: Look up a Banner person by uniqueid
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/uniqueId=doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element uniqueId equal to "doej"
    And the subject has an element pidm equal to "123456"
    And the subject has an element bannerId equal to "+00123456"

  Scenario: Look up a Banner person by uid
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/uid=doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element uniqueId equal to "doej"
    And the subject has an element pidm equal to "123456"
    And the subject has an element bannerId equal to "+00123456"

  Scenario: Look up a Banner person by pidm
    Given a REST client
    And a token for the doej user
    When I make a GET request for /extension/person/pidm=123456
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element uniqueId equal to "doej"
    And the subject has an element pidm equal to "123456"
    And the subject has an element bannerId equal to "+00123456"

