<?php
namespace MiamiOH\RESTng\Service\Tests\Unit;

use Exception;

class BannerUtilTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $bannerUtil;

    private $bannerIdFactory;

    private $mockId;
    private $mockGetIdResult = false;
    private $testIdValue = '';

    protected function setUp()
    {

        $this->mockId = '';
        $this->mockGetIdResult = false;
        $this->testIdValue = '';

        $this->bannerIdFactory = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerIdFactory')
            ->setMethods(array('newBannerId'))
            ->getMock();

        $this->bannerIdFactory->method('newBannerId')
            ->will($this->returnCallback(array($this, 'newBannerIdMock')));

        $this->bannerUtil = new \MiamiOH\RESTng\Service\Extension\BannerUtil();

        $this->bannerUtil->setBannerIdFactory($this->bannerIdFactory);

    }

    public function testGetIdByPidm()
    {

        $this->mockGetIdResult = true;

        $this->mockId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('loadIdByPidm'))
            ->getMock();

        $this->mockId->method('loadIdByPidm')
            ->with($this->callback(array($this, 'loadIdParam')))
            ->will($this->returnCallback(array($this, 'loadIdMock')));

        $person = $this->bannerUtil->getId('pidm', 123456);

        $this->assertEquals($this->mockId, $person);

    }

    public function testGetIdByBannerId()
    {

        $this->mockGetIdResult = true;

        $this->mockId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('loadIdByBannerId'))
            ->getMock();

        $this->mockId->method('loadIdByBannerId')
            ->with($this->callback(array($this, 'loadIdParam')))
            ->will($this->returnCallback(array($this, 'loadIdMock')));

        $person = $this->bannerUtil->getId('bannerId', '+00123456');

        $this->assertEquals($this->mockId, $person);

    }

    public function testGetIdByUid()
    {

        $this->mockGetIdResult = true;

        $this->mockId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('loadIdByUid'))
            ->getMock();

        $this->mockId->method('loadIdByUid')
            ->with($this->callback(array($this, 'loadIdParam')))
            ->will($this->returnCallback(array($this, 'loadIdMock')));

        $person = $this->bannerUtil->getId('uid', 'doej');

        $this->assertEquals($this->mockId, $person);

    }

    public function testGetIdByUniqueId()
    {

        $this->mockGetIdResult = true;

        $this->mockId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('loadIdByUid'))
            ->getMock();

        $this->mockId->method('loadIdByUid')
            ->with($this->callback(array($this, 'loadIdParam')))
            ->will($this->returnCallback(array($this, 'loadIdMock')));

        $person = $this->bannerUtil->getId('uniqueId', 'doej');

        $this->assertEquals($this->mockId, $person);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage No matches for 'szbuniq_unique_id = DOEJ'
     */
    public function testGetIdByUidNotFound()
    {

        $this->mockGetIdResult = false;

        $this->mockId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('loadIdByUid'))
            ->getMock();

        $this->mockId->method('loadIdByUid')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = DOEJ'")));

        $person = $this->bannerUtil->getId('uid', 'doej');

        $this->assertNull($person);

    }

    public function newBannerIdMock()
    {
        return $this->mockId;
    }

    public function loadIdParam($subject)
    {
        $this->testIdValue = $subject;
        return true;
    }

    public function loadIdMock()
    {
        return $this->mockGetIdResult;
    }
}