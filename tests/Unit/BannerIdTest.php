<?php
namespace MiamiOH\RESTng\Service\Tests\Unit;

class BannerIdTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $bannerId;

    private $dbh;

    private $mockResponses = [];
    private $executedQueries = [];

    protected function setUp()
    {

        $this->mockResponses = [];
        $this->executedQueries = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'queryfirstrow_assoc'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $this->bannerId = new \MiamiOH\RESTng\Service\Extension\BannerId();

        $this->bannerId->setDatabase($db);

    }

    public function testLoadIdByPidm()
    {

        $this->mockResponses['queryfirstcolumn'] = '1';

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->bannerId->loadIdByPidm(123456);

        $this->assertTrue(stripos($this->executedQueries['queryfirstcolumn']['sql'], 'szbuniq_pidm = ?') !== false,
            'Query from szbuniq using szbuniq_pidm');
        $this->assertEquals(123456, $this->executedQueries['queryfirstcolumn']['params'],
            'Query param is given pidm');

    }

    public function testLoadIdByBannerId()
    {

        $this->mockResponses['queryfirstcolumn'] = '1';

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->bannerId->loadIdByBannerId('+00123456');

        $this->assertTrue(stripos($this->executedQueries['queryfirstcolumn']['sql'], 'szbuniq_banner_id = ?') !== false,
            'Query from szbuniq using szbuniq_banner_id');
        $this->assertEquals('+00123456', $this->executedQueries['queryfirstcolumn']['params'],
            'Query param is given banner id');

    }

    public function testLoadIdByUid()
    {

        $this->mockResponses['queryfirstcolumn'] = '1';
        $this->mockResponses['queryfirstrow_assoc'] = [
            'uniqueid' => 'doej',
            'pidm' => 123456,
            'bannerid' => '+00123456'
        ];

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocSql')),
                $this->callback(array($this, 'queryfirstrow_assocParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $loaded = $this->bannerId->loadIdByUid('doej');

        $this->assertTrue($loaded);

        $this->assertTrue(stripos($this->executedQueries['queryfirstcolumn']['sql'], 'szbuniq_unique_id = ?') !== false,
            'Query count from szbuniq using szbuniq_unique_id');
        $this->assertEquals(strtoupper('doej'), $this->executedQueries['queryfirstcolumn']['params'],
            'Query count param is given uniqueid');

        $this->assertTrue(stripos($this->executedQueries['queryfirstrow_assoc']['sql'], 'szbuniq_unique_id = ?') !== false,
            'Query from szbuniq using szbuniq_unique_id');
        $this->assertEquals(strtoupper('doej'), $this->executedQueries['queryfirstrow_assoc']['params'],
            'Query param is given uniqueid');

        $this->assertEquals('doej', $this->bannerId->getUniqueId());
        $this->assertEquals(123456, $this->bannerId->getPidm());
        $this->assertEquals('+00123456', $this->bannerId->getBannerId());
    }

    public function testLoadIdByUniqueId()
    {

        $this->mockResponses['queryfirstcolumn'] = '1';
        $this->mockResponses['queryfirstrow_assoc'] = [
            'uniqueid' => 'doej',
            'pidm' => 123456,
            'bannerid' => '+00123456'
        ];

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocSql')),
                $this->callback(array($this, 'queryfirstrow_assocParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $loaded = $this->bannerId->loadIdByUniqueId('doej');

        $this->assertTrue($loaded);

        $this->assertTrue(stripos($this->executedQueries['queryfirstcolumn']['sql'], 'szbuniq_unique_id = ?') !== false,
            'Query count from szbuniq using szbuniq_unique_id');
        $this->assertEquals(strtoupper('doej'), $this->executedQueries['queryfirstcolumn']['params'],
            'Query count param is given uniqueid');

        $this->assertTrue(stripos($this->executedQueries['queryfirstrow_assoc']['sql'], 'szbuniq_unique_id = ?') !== false,
            'Query from szbuniq using szbuniq_unique_id');
        $this->assertEquals(strtoupper('doej'), $this->executedQueries['queryfirstrow_assoc']['params'],
            'Query param is given uniqueid');

        $this->assertEquals('doej', $this->bannerId->getUniqueId());
        $this->assertEquals(123456, $this->bannerId->getPidm());
        $this->assertEquals('+00123456', $this->bannerId->getBannerId());
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage No matches for 'szbuniq_unique_id = DOEJ'
     */
    public function testLoadIdByUniqueIdNotFound()
    {

        $this->mockResponses['queryfirstcolumn'] = '0';

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->never())->method('queryfirstrow_assoc');

        $this->bannerId->loadIdByUniqueId('doej');

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Too many matches for 'szbuniq_unique_id = DOEJ'
     */
    public function testLoadIdByUidTooManyFound()
    {

        $this->mockResponses['queryfirstcolumn'] = '2';

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnSql')),
                $this->callback(array($this, 'queryfirstcolumnParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->never())->method('queryfirstrow_assoc');

        $this->bannerId->loadIdByUid('doej');

    }

    public function queryfirstcolumnSql($subject)
    {
        $this->executedQueries['queryfirstcolumn']['sql'] = $subject;
        return true;
    }

    public function queryfirstcolumnParams($subject)
    {
        $this->executedQueries['queryfirstcolumn']['params'] = $subject;
        return true;
    }

    public function queryfirstcolumnMock()
    {
        return $this->mockResponses['queryfirstcolumn'];
    }

    public function queryfirstrow_assocSql($subject)
    {
        $this->executedQueries['queryfirstrow_assoc']['sql'] = $subject;
        return true;
    }

    public function queryfirstrow_assocParams($subject)
    {
        $this->executedQueries['queryfirstrow_assoc']['params'] = $subject;
        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->mockResponses['queryfirstrow_assoc'];
    }
}