insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
  values (lower('doej'), 1, lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>'Hello123')))),
      sysdate + 365);

insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
  values (lower('smithd'), 1, lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>'Hello123')))),
      sysdate + 365);
