# RESTng Extensions

This project provides utility classes for use in RESTng. The RESTng core should not contain any Banner or other system specific details. However, there is value in providing some such functionality within the framework. This extension project allows us to utilize RESTng's service declaration capability to override core services and add instance specific services.

The project currently provides two features:

1. MUID Alternate Key Resolution for SELF Authorization
2. BannerId Resolution

## Installation

The initial project will be installed using the legacy technique of cloning from git and symlinking into the RESTng Services directory.

For example:

    export RESTNGEXT_HOME=/path/to/dir
    git clone git@git.itapps.miamioh.edu:solution-delivery-dev/restng-extensions.git $RESTNGEXT_HOME
    ln -s $RESTNGEXT_HOME/rest/src/Service $RESTNG_HOME/RESTng/Service/Extension
    
## MUID Alternate Key Resolution for Self Authorization

RESTng provides the ability to declare alternate keys for resource paths. A common scenario in SolDel applications will be to use alternate keys to identify a person for use with Banner records. RESTng also supports the concept of SELF authorization by allowing the resource to specify a path parameter to validate against the current authenticated username. While the combination of these two features works well with Miami's UniqueID, RESTng is unable to resolve and validate anything other than UniqueID for SELF authorization

The MUUser of this project is a drop in replacement for RESTng core User class and can be used to declare a local APIUser service which can resolve alternate forms of the Miami ID value.

Once the RESTng-Extensions project is installed, simply add the following the $RESTNG_HOME/conf/services.php file:

    $app->addService(array(
        'name' => 'APIUser',
        'class' => 'RESTng\Service\Extension\MUUser',
        'description' => 'Provides access to the current API user.',
        'set' => array(
          'api' => array('name' => 'API'),
          'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
          ),
    ));

In your resource defination, you will declare an muid parameter and associated authorization:

    'pattern' => '/extension/person/:muid',
    'params' => array(
        'muid' => array('description' => 'A Miami identifier', 'alternateKeys' => ['uniqueId', 'pidm']),
    ),
    'middleware' => array(
        'authenticate' => array(),
        'authorize' => array(
            array(
                'type' => 'self',
                'param' => 'muid',
            ),
        ),
    ),

*Note* Due to limitations in the Apache mod_rewrite implementation, Banner IDs (aka Plus Numbers) cannot be used as parameters in RESTng paths. The HTTP specification allows for the represenation of spaces using a '+' character and the mod_rewrite rules will allows decode it as such, even if double encoded. If Banner IDs are required as path components, we must establish a guideline for replacing the '+' with another character.

## BannerId Resolution

Since many of our resources deal with people or person related records, resolving identity based on UniqueID, PIDM or Banner ID is a frequent need. This project provides a BannerUtil class which can be injected as a service and used to find and return a BannerId object. The BannerID object will contain all of the key Miami indentifiers for the ID, making it easy to use them in subsequent operations.

Once installed, define the following services in the $RESTNG_HOME/conf/services.php file:

    $app->addService(array(
        'name' => 'MU\BannerUtil',
        'class' => 'RESTng\Service\Extension\BannerUtil',
        'description' => 'Provides utility functions for working with Banner data.',
        'set' => array(
          'bannerIdFactory' => array('type' => 'service', 'name' => 'MU\BannerIdFactory'),
          ),
    ));
    
    $app->addService(array(
        'name' => 'MU\BannerIdFactory',
        'class' => 'RESTng\Service\Extension\BannerIdFactory',
        'description' => 'Provides Banner ID object factory.',
        'set' => array(
          'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
          ),
    
    ));

In your service definition, inject the BannerUtil service using a setter (or a constructor):

    'set' => array(
        'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
    )

Your resource method may then use the BannerUtil service to fetch a BannerId object based a key and value. For example:

    $person = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
        $request->getResourceParam('muid'));

    $uniqueId = $person->getUniqueId();
    $pidm = $person->getPidm();
    $bannerId = $person->getBannerId();

The BannerUtil::getId method takes two parameters. The first is the key field which must be one of 'uniqueId', 'pidm' or 'bannerId'. The second is the value to look up.