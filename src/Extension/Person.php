<?php

namespace MiamiOH\RESTng\Service\Extension;

class Person extends \MiamiOH\RESTng\Service
{

    private $database;
    private $dbh;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('MUWS_SEC_PROD');
        $this->dbh->mu_trigger_error = false;
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getPerson()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
            $request->getResourceParam('muid'));

        $payload = [
            'uniqueId' => $bannerId->getUniqueId(),
            'pidm' => $bannerId->getPidm(),
            'bannerId' => $bannerId->getBannerId(),
        ];

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

}
