<?php

namespace MiamiOH\RESTng\Service\Extension;

class BannerUtil
{

    /** @var \MiamiOH\RESTng\Service\Extension\BannerIdFactory $bannerPersonFactory */
    private $bannerIdFactory;

    /**
     * @param $factory
     */
    public function setBannerIdFactory($factory)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerIdFactory $factory */
        $this->bannerIdFactory = $factory;
    }

    public function getId($key, $muid)
    {

        $id = $this->bannerIdFactory->newBannerId();

        switch ($key) {
            case 'uid':
            case 'uniqueId':
                $id->loadIdByUniqueId($muid);
                break;

            case 'pidm':
                $id->loadIdByPidm($muid);
                break;

            case 'bannerId':
                $id->loadIdByBannerId($muid);
                break;

        }

        return $id;
    }

}
