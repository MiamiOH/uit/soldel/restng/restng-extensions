<?php

namespace MiamiOH\RESTng\Service\Extension;

class MUUser extends \MiamiOH\RESTng\Util\User
{

    private $database = '';
    private $dbh = '';

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('MUWS_SEC_PROD');
    }

    public function isSelfAccess($paramName, $callable = '')
    {
        if (parent::isSelfAccess($paramName, $callable)) {
            return true;
        }

        /*
         * If the given route param contains an '=', then it is an alternate key
         * syntax and needs to be handled here. Unfortunately, the Slim
         * middleware call happens before RESTng has parsed the request, so we
         * need to extract the key and value ourselves.
         */
        $routeParams = $this->api->getAppInstance()->router()->getCurrentRoute()->getParams();

        if (!isset($routeParams[$paramName])) {
            return false;
        }

        $paramValue = $routeParams[$paramName];

        $this->log->debug("param name: $paramName");
        $this->log->debug("param value: $paramValue");

        if (strpos($paramValue, '=') !== false) {

            $currentUsername = $this->getUsername();

            if (!$currentUsername) {
                return false;
            }

            $exists = $this->dbh->queryfirstcolumn('
                select count(*)
                    from szbuniq
                    where szbuniq_unique_id = upper(?)
                ', $currentUsername);

            $this->log->debug("user exists: $exists");
            if ($exists) {
                $personData = $this->dbh->queryfirstrow_assoc('
                        select lower(szbuniq_unique_id) as uniqueid, szbuniq_banner_id as bannerid, szbuniq_pidm as pidm 
                            from szbuniq
                            where szbuniq_unique_id = upper(?)
                    ', $currentUsername);

                list($key, $value) = explode('=', $paramValue);

                // Clean up some case and historic keys
                $personData['uniqueId'] = $personData['uniqueid'];
                $personData['uid'] = $personData['uniqueid'];

                return isset($personData[$key]) && (string)$personData[$key] === (string)$value;
            }
        }

        return false;
    }
}