<?php

namespace MiamiOH\RESTng\Service\Extension;

class BannerId
{

    private $database = '';
    /** @var \MiamiOH\RESTng\Legacy\DB\DBH\OCI8 $dbh */
    private $dbh = '';

    private $uniqueId = '';
    private $pidm = 0;
    private $bannerId = '';

    public function setDatabase($database)
    {
        $this->database = $database;

        $this->dbh = $this->database->getHandle('MUWS_SEC_PROD');
    }

    /**
     * @param $uid
     *
     * @return bool
     * @throws BannerIdNotFound
     * @throws BannerIdTooManyMatches
     */
    public function loadIdByUniqueId($uniqueId)
    {
        return $this->loadId('szbuniq_unique_id', strtoupper($uniqueId));
    }

    /**
     * @param $uid
     *
     * @return bool
     * @throws BannerIdNotFound
     * @throws BannerIdTooManyMatches
     */
    public function loadIdByUid($uniqueId)
    {
        return $this->loadIdByUniqueId($uniqueId);
    }

    /**
     * @param $pidm
     *
     * @return bool
     * @throws BannerIdNotFound
     * @throws BannerIdTooManyMatches
     */
    public function loadIdByPidm($pidm)
    {
        return $this->loadId('szbuniq_pidm', $pidm);
    }

    /**
     * @param $bannerId
     *
     * @return bool
     * @throws BannerIdNotFound
     * @throws BannerIdTooManyMatches
     */
    public function loadIdByBannerId($bannerId)
    {
        return $this->loadId('szbuniq_banner_id', $bannerId);
    }

    public function getUniqueId()
    {
        return $this->uniqueId;
    }

    public function getPidm()
    {
        return $this->pidm;
    }

    public function getBannerId()
    {
        return $this->bannerId;
    }

    private function loadId($column, $value)
    {
        $count = (int)$this->dbh->queryfirstcolumn("
                select count(*)
                    from szbuniq
                    where $column = ? 
            ", $value);

        if ($count === 0) {
            throw new BannerIdNotFound("No matches for '$column = $value'");
        } elseif ($count > 1) {
            throw new BannerIdTooManyMatches("Too many matches for '$column = $value'");
        }

        $record = $this->dbh->queryfirstrow_assoc("
            select lower(szbuniq_unique_id) as uniqueid, szbuniq_pidm as pidm, szbuniq_banner_id as bannerid
                from szbuniq
                where $column = ? 
        ", $value);

        $this->uniqueId = $record['uniqueid'];
        $this->pidm = $record['pidm'];
        $this->bannerId = $record['bannerid'];

        return true;
    }
}
