<?php

namespace MiamiOH\RESTng\Service\Extension;

class BannerIdFactory
{

    private $database;

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function newBannerId()
    {

        $id = new \MiamiOH\RESTng\Service\Extension\BannerId();
        $id->setDatabase($this->database);

        return $id;
    }

}
